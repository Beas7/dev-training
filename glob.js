// @ts-check

/**
 * @param {string | Array} i
 * @returns {void}
 */
const printResult = (i) => {
  // @ts-ignore
  document.querySelector('#result').innerText = i;
};

export default printResult;
