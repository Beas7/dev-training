// @ts-check
/**
 * @param  {...Array<number>} args
 * @returns {Array<number>}
 */
const sym = (...args) => [...new Set(args.reduce(diff))].sort();
/**
 * @param {Array<number>} arr1
 * @param {Array<number>} arr2
 * @returns {Array<number>}
 */
const diff = (arr1, arr2) => [...arr1.filter((el) => !arr2.includes(el)), ...arr2.filter((el) => !arr1.includes(el))];

module.exports = sym;
