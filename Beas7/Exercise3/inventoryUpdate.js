// @ts-check
/**
 * @param {Array<Array<number|string>>} curInv
 * @param {Array<Array<number|string>>} newInv
 * @returns {Array}
 */
const updateInventory = (curInv, newInv) => {
  /** @type {Map} */
  var inv = new Map();

  [...curInv, ...newInv].forEach((item) => {
    if (inv.has(item[1])) inv.set(item[1], inv.get(item[1]) + item[0]);
    else inv.set(item[1], item[0]);
  });

  return [...inv].map((item) => [item[1], item[0]]).sort((a, b) => (a[1] > b[1] ? 1 : -1));
};

module.exports = updateInventory;

/**
 * @param {Array<Array<number|string>>} curInv
 * @param {Array<Array<number|string>>} newInv
 * @returns {Array}
 */
function updateInventory2(curInv, newInv) {
  /** @type {Array} */
  let temp = [];
  for (let i = 0; i < curInv.length; i++) {
    temp.push(curInv[i][1]);
  }
  newInv.map((item) => {
    if (!temp.includes(item[1])) {
      curInv.push(item);
    }
    if (temp.includes(item[1])) {
      /** @type {number} */
      let index = temp.indexOf(item[1]);
      // @ts-ignore
      curInv[index][0] += item[0];
    }
  });
  return curInv.sort((a, b) => (a[1] > b[1] ? 1 : -1));
}
