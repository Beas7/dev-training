// @ts-check
const reverseString = require('./reverseString');

test('text', () => {
  /* reverseString("hello") should return a string. */
  expect(typeof reverseString('hello')).toBe('string');
  /* reverseString("hello") should return the string olleh.*/
  expect(reverseString('hello')).toBe('olleh');
  /* reverseString("Howdy") should return the string ydwoH. */
  expect(reverseString('Howdy')).toBe('ydwoH');
  /* reverseString("Greetings from Earth") should return the string htraE morf sgniteerG. */
  expect(reverseString('Greetings from Earth')).toBe('htraE morf sgniteerG');
});
