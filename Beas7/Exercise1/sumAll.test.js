// @ts-check
const sumAll = require('./sumAll');

test('Sum All', () => {
  expect(typeof sumAll([1, 4])).toBe('number');
  expect(sumAll([1, 4])).toBe(10);
  expect(sumAll([4, 1])).toBe(10);
  expect(sumAll([5, 10])).toBe(45);
  expect(sumAll([10, 5])).toBe(45);
});
