// @ts-check
/**
 * @param {Array<number>} arr
 * @returns {number}
 */
const sumAll = (arr) => {
  try {
    let /** @type  {number} */ biggest = Math.max(...arr),
      /** @type  {number} */ smallest = Math.min(...arr),
      /** @type  {number} */ res = 0;
    for (let i = smallest; i <= biggest; i++) res += i;
    return res;
  } catch (e){
    throw "sumAllException";
  } finally {
    
  }
};

module.exports = sumAll;
