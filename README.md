# DEV-Training

## Getting started

```
npm i
npm run test
```

```
npm i
npm run dev
```

## Completing exercises

You should use **JSDoc** along with **ts-check** to ensure the code is documented plus that there are no type errors.

You are expected to use the **functional programming paradigm** progressively better as you complete more exercises.

You are expected to use **TDD** as development practice. It is recommended to code the tests beforehand. Nonetheless it's up to you choosing to do
**test-first** or **code-first**.

## Add your files

```
cd DEV-Training
git remote add origin https://gitlab.com/Beas7/dev-training.git
git branch -M main
git push -uf origin main
```

## Collaborate with your team

- [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
