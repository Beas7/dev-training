const sym = require('./exercise2');

test('array', () => {
  // sym([1, 2, 3], [5, 2, 1, 4]) should return [3, 4, 5].
  expect( sym([1, 2, 3], [5, 2, 1, 4])).toStrictEqual([3, 4, 5]);
 
});