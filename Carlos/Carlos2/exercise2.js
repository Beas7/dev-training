// @ts-check
/**
 * @param {array} param1
 * * @param {array} param2
 * @returns {array}
 */
function sym(param1, param2) {
  const calcLength = () => {
    let newNotRepeat1 = [];
    let newNotRepeat2 = [];
    for (let index = 0; index < param2.length; index++) {
      const element = param1[index];
      if (newNotRepeat1.includes(element) == false) {
        newNotRepeat1.push(element);
      }
    }
    for (let index = 0; index < param1.length; index++) {
      const element = param2[index];
      if (newNotRepeat2.includes(element) == false) {
        newNotRepeat2.push(element);
      }
    }
    var arr = [newNotRepeat1.length, newNotRepeat2.length];
    return Math.max(...arr);
  };
  const lengthFor = calcLength();

  let numbers = [];
  for (let index = 0; index < lengthFor; index++) {
    const element1 = param1[index];
    const element2 = param2[index];

    if (param2.includes(element1) == false) {
      if (numbers.includes(element1) == false) {
        numbers.push(element1);
      }
    }
    if (param1.includes(element2) == false) {
      if (numbers.includes(element2) == false) {
        numbers.push(element2);
      }
    }
  }

  const filterNumbers = numbers.filter((num) => {
    return num != undefined;
  });

  const result = filterNumbers.sort();

  return result;
}

sym([1, 1, 2, 5], [2, 2, 3, 5]);

module.exports = sym;

// for (let index = 0; index < lengthFor; index++) {
//     const element3 = param3[index]
//     const element4 = filterNumbers[index]

//     if (param3.includes(element3) == false) {
//         if (numbers.includes(element3)  == false) {
//             numbers.push(element3)
//         }
//     }
//     if (filterNumbers.includes(element4) == false) {
//         if (numbers.includes(element4) == false) {
//             numbers.push(element4)
//         }
//     }

// }

// const filterNumbers2 = numbers.filter( (num) => {
//     return num != undefined
// })
