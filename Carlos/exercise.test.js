const sumNumArray = require('./exercise1');

test('num', () => {
  // sumAll([1, 4])` should return a `number
  expect(typeof sumNumArray([1, 4])).toBe('number');
  //sumAll([1, 4])` should return `10`.
  expect(sumNumArray([1, 4])).toBe(10);
  //sumAll([4, 1])` should return `10`
  expect(sumNumArray([4, 1])).toBe(10);
  //sumAll([5, 10])` should return `45`
  expect(sumNumArray([5, 10])).toBe(45);
  //sumAll([10, 5])` should return `45`
  expect(sumNumArray([10, 5])).toBe(45);
 
});



