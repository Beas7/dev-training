// @ts-check
/**
 * @param {array} array
 * @returns {number} 
 */
 function sumNumArray(array) {
    let min =  Math.min(array[0],array[1])
    let max =  Math.max(array[0],array[1])
    let sum = 0 
    for (let index = min; index <= max; index++) {
        sum = sum + index   
    }
    return sum 
  }
 
  module.exports = sumNumArray;