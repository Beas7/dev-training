// @ts-check
/**
 * @param {array} array
 * @returns {array} 
 */
 function updateInventory(p1, p2) {
    
    let acum = []
    p1.map((item, indexp1) => {
      let amount1 =  item[0] 
      let name1   =  item[1]
      p2.map((item2, indexP2) => {
         let amount2 =  item2[0] 
         let name2 =    item2[1]
         if (name1 == name2) {
            let sum1 = amount1 + amount2
            acum.push([sum1, name1]) 
            p1.splice(indexp1, 1);
            p2.splice(indexP2, 1);
         } 
      })
      
    }) 
    let acumulator = p1.concat(p2).concat(acum)
    let result = acumulator.sort((a,b) => {
       if (a[1] == b[1]) {
          return 0
       }
       if (a[1] < b[1]) {
          return -1
       }
       return 1
    })
    return result 
  }
    
  module.exports = updateInventory;


