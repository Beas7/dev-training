// @ts-check
/**
 * @param {Array<Number>} arr1, arr2
 * @returns {Array<Number>}
 */
 function symDiff() {
    let sets = [], result = [];
    // make copy of arguments into an array
    let args = Array.prototype.slice.call(arguments, 0);
    args.forEach((arr) => {
        sets.push(new Set(arr));
    });

    args.forEach((array, arrayIndex) => {
        // iterate each item in the array
        array.forEach((item) => {
            let found = false;
            // iterate each set (use a plain for loop so it's easier to break)
            for (let setIndex = 0; setIndex < sets.length; setIndex++) {
                // skip the set from our own array
                if (setIndex !== arrayIndex) {
                    if (sets[setIndex].has(item)) {
                        // if the set has this item
                        found = true;
                        break;
                    }
                }
            }
            if (!found) {
                result.push(item);
            }
        });
    });
    return result;
}

module.exports = symDiff;