// @ts-check
const sumAll = require('./sumAll');

test('text', () => {
  /* sumAll("hello") should return a number. */
  expect(typeof sumAll([1, 4])).toBe('number');
  /* sumAll("hello") should return the number 10. */
  expect(sumAll([1, 4])).toBe(10);
  /* sumAll("hello") should return the number 10.*/
  expect(sumAll([1, 4])).toBe(10);
  /* sumAll("Howdy") should return the number 45. */
  expect(sumAll([5, 10])).toBe(45);
  /* sumAll("Greetings from Earth") should return the number 45. */
  expect(sumAll([10, 5])).toBe(45);
});