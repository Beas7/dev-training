// @ts-check
/**
 * @param {Array<Number>} arr
 * @returns {number}
 */
 function sumAll(arr) {
  let big 
  let small
  let sum = 0

  //find highest num and assign respective values to variables
   if(arr[0] > arr[1]){
      big = arr[0]
      small = arr[1]
   } else if(arr[0] == arr[1]){
     return arr[0] + arr[1]
   } else{
    small = arr[0]
    big = arr[1]
   }

   //Sum the two provided values plus the values between them
   for(let i=small; i <= big; i++){
     sum += i
   }
    return (sum)
  }
  
  module.exports = sumAll;
  