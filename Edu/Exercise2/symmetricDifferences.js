// @ts-check
/**
 * @param {Array<Array<number>>} args
 * @returns {Array<number>}
 */
 function sym(...args) {
   if (Array.isArray(args) && args.length >= 2) {
     let i = 0;
     let result = args[i];
     while (args.length-1 > i) {
      result = symTwoArrays(result,args[++i]);
    }
     return result;
    }
  return undefined;
 }

 /**
 * @param {Array<number>} firstArray
 * @param {Array<number>} secondArray
 * @returns {Array<number>} result
 */
  function symTwoArrays(firstArray, secondArray) {
    let result = [];
    /*firstArray.forEach((element) => {
        if(!secondArray.includes(element) && !result.includes(element)) result.push(element)});
    secondArray.forEach((element) => {
      if(!firstArray.includes(element) && !result.includes(element)) result.push(element)});
*/
   for (let i=0, j=0; i<firstArray.length || j<secondArray.length; ){
     if(i<firstArray.length) {
      if(!secondArray.includes(firstArray[i]) && !result.includes(firstArray[i])) result.push(firstArray[i]);
      i++;
     }
     if(j<secondArray.length) {
      if(!firstArray.includes(secondArray[j]) && !result.includes(secondArray[j])) result.push(secondArray[j]);
      j++;
     }
   }
   return result.sort();
  }
 
 module.exports = sym;
