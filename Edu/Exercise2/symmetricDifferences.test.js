// @ts-check
const sym = require('./symmetricDifferences');

test('symmetricDifferencesEdu', () => {
  /*sym([1, 2, 3], [5, 2, 1, 4]) should return [3, 4, 5].*/
  expect(sym([1, 2, 3], [5, 2, 1, 4])).toStrictEqual([3, 4, 5]);
  /*sym([1, 2, 3], [5, 2, 1, 4]) should contain only three elements.*/
  expect(sym([1, 2, 3], [5, 2, 1, 4]).length).toBe(3);
  /*sym([1, 2, 3, 3], [5, 2, 1, 4]) should return [3, 4, 5].*/
  expect(sym([1, 2, 3, 3], [5, 2, 1, 4])).toStrictEqual([3, 4, 5]);
  /*sym([1, 2, 3, 3], [5, 2, 1, 4]) should contain only three elements.*/
  expect(sym([1, 2, 3], [5, 2, 1, 4]).length).toBe(3);
  /*sym([1, 2, 3], [5, 2, 1, 4, 5]) should return [3, 4, 5].*/
  expect(sym([1, 2, 3, 3], [5, 2, 1, 4, 5])).toStrictEqual([3, 4, 5]);
  /*sym([1, 2, 3], [5, 2, 1, 4, 5]) should contain only three elements.*/
  expect(sym([1, 2, 3, 3], [5, 2, 1, 4, 5]).length).toBe(3);
  /*sym([1, 2, 5], [2, 3, 5], [3, 4, 5]) should return [1, 4, 5]*/
  expect(sym([1, 2, 5], [2, 3, 5], [3, 4, 5])).toStrictEqual([1, 4, 5]);
  /*sym([1, 2, 5], [2, 3, 5], [3, 4, 5]) should contain only three elements.*/
  expect(sym([1, 2, 5], [2, 3, 5], [3, 4, 5]).length).toBe(3);
  /*sym([1, 1, 2, 5], [2, 2, 3, 5], [3, 4, 5, 5]) should return [1, 4, 5].*/
  expect(sym([1, 2, 5], [2, 3, 5], [3, 4, 5, 5])).toStrictEqual([1, 4, 5]);
  /*sym([1, 1, 2, 5], [2, 2, 3, 5], [3, 4, 5, 5]) should contain only three elements.*/
  expect(sym([1, 2, 5], [2, 3, 5], [3, 4, 5]).length).toBe(3);
  /*sym([3, 3, 3, 2, 5], [2, 1, 5, 7], [3, 4, 6, 6], [1, 2, 3]) should return [2, 3, 4, 6, 7].*/
  expect(sym([3, 3, 3, 2, 5], [2, 1, 5, 7], [3, 4, 6, 6], [1, 2, 3])).toStrictEqual([2, 3, 4, 6, 7]);
  /*sym([3, 3, 3, 2, 5], [2, 1, 5, 7], [3, 4, 6, 6], [1, 2, 3]) should contain only five elements.*/
  expect(sym([3, 3, 3, 2, 5], [2, 1, 5, 7], [3, 4, 6, 6], [1, 2, 3]).length).toBe(5);
  /*sym([3, 3, 3, 2, 5], [2, 1, 5, 7], [3, 4, 6, 6], [1, 2, 3], [5, 3, 9, 8], [1]) should return [1, 2, 4, 5, 6, 7, 8, 9].*/
  expect(sym([3, 3, 3, 2, 5], [2, 1, 5, 7], [3, 4, 6, 6], [1, 2, 3], [5, 3, 9, 8], [1])).toStrictEqual([1, 2, 4, 5, 6, 7, 8, 9]);
  /*sym([3, 3, 3, 2, 5], [2, 1, 5, 7], [3, 4, 6, 6], [1, 2, 3], [5, 3, 9, 8], [1]) should contain only eight elements.*/
  expect(sym([3, 3, 3, 2, 5], [2, 1, 5, 7], [3, 4, 6, 6], [1, 2, 3], [5, 3, 9, 8], [1]).length).toBe(8);
});
