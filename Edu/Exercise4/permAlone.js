// @ts-check
/**
 * @param {string} cadena
 * @param {string} permutacion
 * @returns {number}
 */
 const permTogether = (cadena, permutacion) => {
    let count = 0;
    if(cadena.length > 0) {
        for (let i=0; i< cadena.length; i++) {
            if(permutacion.length == 0 || permutacion[permutacion.length-1] != cadena[i]) {
                let nuevaCadena = cadena.substring(0,i).concat(cadena.substring(1+i,cadena.length));
                let nuevaPermutacion = permutacion + cadena[i];
                count += permTogether(nuevaCadena, nuevaPermutacion)
            }
        }
    } else {
        return 1;
    }
    return count;
}

/**
 * @param {string} cadena
 * @returns {number}
 */
 const permAlone = (cadena) => {
    if (cadena.length > 0) {
        return permTogether(cadena, '');
    } else {
        return 0;
    }
}

module.exports = permAlone;
