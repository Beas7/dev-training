// @ts-check
/**
 * @param {Array<Array<number|string>>} arr1
 * @param {Array<Array<number|string>>} arr2
 * @returns {Array<Array<number|string>>}
 */
const updateInventory = (arr1, arr2) => {
  let result = arr1.slice();
  arr2.forEach((el2) => {
  	let index = result.findIndex((el1) => el1[1] > el2[1])
	if (index > -1) {
	  if (index == 0 || result[index-1][1] != el2[1]) {
	    result.splice(index, 0, el2)
	  } else result[index-1][0]+= el2[0]
    } else {
	  result.push(el2)
	}
  });
  return result;
}

module.exports = updateInventory;
