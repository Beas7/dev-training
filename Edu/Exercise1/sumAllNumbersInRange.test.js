// @ts-check
const sumAllNumbersInRange = require('./sumAllNumbersInRange');

test('text', () => {
  /* - `sumAll([1, 4])` should return a `number`. */
  expect(typeof sumAllNumbersInRange([1, 4])).toBe('number');
  /* - `sumAll([1, 4])` should return `10`.*/
  expect(sumAllNumbersInRange([1, 4])).toBe(10);
  /* - `sumAll([4, 1])` should return `10`. */
  expect(sumAllNumbersInRange([4, 1])).toBe(10);
  /* - `sumAll([5, 10])` should return `45`. */
  expect(sumAllNumbersInRange([10, 5])).toBe(45);
  /*- `sumAll([10, 5])` should return `45`. */
  expect(sumAllNumbersInRange([10, 5])).toBe(45);
  /*- `sumAll([5, 5])` should return `10`. */
  expect(sumAllNumbersInRange([5, 5])).toBe(10);
  /*- `sumAll([-5, 3])` should return `-9`. */
  expect(sumAllNumbersInRange([-5,3])).toBe(-9);
  /*- `sumAll([5, 5, 3])` should return `undefined`. */
  expect(sumAllNumbersInRange([5, 5, 3])).toBe(undefined);
  /*- `sumAll([5, 5, 3])` should return `undefined`. */
  expect(sumAllNumbersInRange([5])).toBe(undefined);
  /*- `sumAll('ab')` should return `undefined`. */
  expect(sumAllNumbersInRange('ab')).toBe(undefined);
  /*- `sumAll(['a','b'])` should return `undefined`. <--- I HAVE A FUCKING DOUBT BECAUSE I GUESS THIS SHOULD NOT BE CONTEMPLATED AT ALL AS THE EXAMPLE ABOVE*/
  //expect(sumAllNumbersInRange(['a','b'])).toBe(undefined);
});
