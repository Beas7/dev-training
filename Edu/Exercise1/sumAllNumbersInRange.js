// @ts-check
/**
 * @param {Array<number>} array
 * @returns {number}
 */
function sumAllNumbersInRange(array) {
  if (array && Array.isArray(array) && array.length == 2) {
    let i = array[0];
    let sum = i;
    do {
      if (array[0] > array[1]) {
        i--;
      } else if (array[0] < array[1]) {
        i++;
      }
      sum += i;
    } while (i != array[1]);
    return sum;
  }
  return undefined;
}

module.exports = sumAllNumbersInRange;
